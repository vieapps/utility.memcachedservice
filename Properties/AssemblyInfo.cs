﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: ComVisible(false)]

[assembly: AssemblyTitle("VIEApps NGX Memcached")]
[assembly: AssemblyCompany("VIEApps.net")]
[assembly: AssemblyProduct("VIEApps NGX")]
[assembly: AssemblyCopyright("© 2018 VIEApps.net")]

[assembly: AssemblyVersion("10.2.2.1809")]
[assembly: AssemblyFileVersion("10.2.2.1809")]
[assembly: AssemblyInformationalVersion("v10.2.netframework+rev: 2018.09.30")]
