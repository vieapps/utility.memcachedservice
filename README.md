# Utility.memcachedService

## General

This is a wrapper for running memcached Server for Windows as a Windows service, its required .NET Framework 4.5

## Resources

### Windows:

You can download **memcached Server for Windows v1.4.5** from Northscale Labs:

- 64-bits version: http://downloads.northscale.com/memcached-1.4.5-amd64.zip
- 32-bits version: http://downloads.northscale.com/memcached-1.4.5-x86.zip

### macOS and Linux:

Please ask Mr. Google ;-)